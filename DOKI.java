import	java.io.*;
import	java.util.*;	

public	class DOKI 
{
	public static EKG user;
	public static ArrayList<EKG> patients;
	public static int cnt_match_thr;

	public static void main(String args[])
	{
		if(args.length<4)
		{
			System.out.println("usage : [signal value match threshold (>=0,<1)] [signal count match threshold (>1)] [path_to_user_EKG] [path_to_patient_0_EKG] ...");
			System.exit(1);
		} 

		cnt_match_thr = Integer.parseInt(args[1]);
		Signal.val_match_thr = Float.parseFloat(args[0]);

		/* create user and patients ****/

		patients = new ArrayList<EKG>();

		try
		{
			user = new EKG(args[2]);

			for(int i=3; i<args.length; i++) {patients.add(new EKG(args[i]));}
		}
		catch(IOException e)
		{
			System.out.println("Creating EKGs failed");
			return;
		}

		/* start search ****************/

		int index = 0;

		while(index!=user.size()) 
		{ 
			System.out.println("Starting search from "+Integer.toString(index));

			index = search(index); 
		}
				
		return;
	}

	public static int search(int start_index)
	{
		for(int threshold=15; threshold>6; threshold--)
		{
			System.out.println("Accuracy threshold set to "+Integer.toString(threshold));			

			int curr_index = start_index;
			int match_count = 100;

			//ArrayList<Integer> last_indices = new ArrayList<Integer>(Collections.nCopies(patients.size(), 0));

			while(match_count>1)
			{
				match_count = 0;

				//System.out.println("Reading entry#"+Integer.toString(curr_index)+" of user");

				Signal val = user.read(curr_index);

				for(int i=0; i<patients.size(); i++)
				{
					//if( patients.get(i).exists(val,threshold) ) {match_count++;}						
					match_count += patients.get(i).count_match(val,threshold);
				}

				curr_index++;

				//System.out.println("* "+Integer.toString(curr_index)+" : "+Integer.toString(match_count));
			}

			// if there was no match, just lower threshold
			if(match_count==0) {continue;}

			// check how many signals matched
			if((curr_index-start_index)<cnt_match_thr)
			{
				// do not print any symptoms
				System.out.println("There was not enough correlation");
				return curr_index;
			}

			// lets find out where the match was 
			for(int i=0; i<patients.size(); i++)
			{
				Signal val = user.read(curr_index-1);

				ArrayList<Integer> matches = patients.get(i).search(val,threshold);

				if(!matches.isEmpty())
				{
				//if(patients.get(i).exists(val,threshold))
				//{
				//	ArrayList<Integer> matches = patients.get(i).search(val,threshold);
				//	assert !matches.isEmpty();

					System.out.println("Closest match was found in index "+Integer.toString(matches.get(0))+"/"+Integer.toString(patients.get(i).size())+" of "+patients.get(i).get_info());
					// print out symptoms with accuracy

					// lets make sure there is no more match
					int match_count2 = 1;

					while( (match_count2!=0) && patients.get(i).is_valid(curr_index) )
					{
						Signal val2 = user.read(curr_index);
						match_count2=patients.get(i).count_match(val2,threshold);  
						curr_index++;
					}

					System.out.println(Integer.toString(curr_index-start_index)+" matching locations");

					return curr_index;
				}
			}
		}

		// no match with any accuracy was found, go to next sample
		return  start_index+1;	
	}
}
