import	java.io.*;
import	java.util.*;

public	class EKG
{
	public static float val_match_thr;

	String info;
	ArrayList<Signal> data;	

	EKG()
	{
		data = new ArrayList<Signal>();
	}

	EKG(String path_to_data) throws IOException
	{ 
		info = path_to_data;

		data = new ArrayList<Signal>(); 

		String str;

		BufferedReader in = null;

		try
		{
			System.out.println("Reading " + path_to_data + "...");
			in = new BufferedReader(new FileReader(path_to_data));

			while((str=in.readLine())!=null)
			{
				Signal val = new Signal();

				String tokens[] = str.split("\\s+");

				for(int j=0; j<15; j++)
					val.data[j] = Integer.parseInt(tokens[j+2]);

				data.add(val);
			}
		}
		finally
		{
			if(in!=null){in.close();}
		}
	}

	void	write(String path_to_file) throws IOException
	{
		FileWriter writer = new FileWriter(path_to_file); 		

		for(int i=0; i<size(); i++)
		{
			writer.write("\t"+Integer.toString(i)+"\t"+read(i).toString()+"\n");
		}

		writer.flush();
		writer.close();
	}

	int	size()				{ return data.size(); }

	void	add(Signal s)			{ data.add(s); }

	Signal	read(int i)			{ return data.get(i); }

	String	get_info()			{ return info; }

	Boolean is_valid(int i)			{ return (i>=0)&&(i<data.size()); }

	Integer	count_match(Signal val)		{ return count_match(val,15); }

	ArrayList<Integer> search(Signal val)	{ return search(val,15); }

	Boolean exists(Signal val, int threshold)
	{
                for(int i=0; i<data.size(); i++)
                        if(read(i).match(val,threshold)) { return true; }		

		return false;
	}

	Integer	count_match(Signal val, int threshold)
	{
		int match_count = 0;

		for(int i=0; i<data.size(); i++)
			if(read(i).match(val,threshold)) { match_count++; }

		return match_count;
	}

	ArrayList<Integer> search(Signal val, int threshold)
	{
		ArrayList<Integer> indices = new ArrayList<Integer>();

		for(int i=0; i<data.size(); i++)	
			if(read(i).match(val,threshold)) { indices.add(i); }	

		return indices;
	}
}
