import  java.io.*;
import  java.util.*;

public  class GenData
{
	public static EKG user;
	public static ArrayList<EKG> patients;

	public static void main(String args[])
	{
		if(args.length<3)
		{
			System.out.println("usage : [path_to_output_EKG] [path_to_patient_0_EKG] [path_to_patient_1_EKG] ...");
			System.exit(1);
		}

		user = new EKG();
		patients = new ArrayList<EKG>();

                try
                {                    
                        for(int i=1; i<args.length; i++) {patients.add(new EKG(args[i]));}
                }
                catch(IOException e)
                {
                        System.out.println("Creating EKGs failed");
                        return;
                }	

		Integer minimum = patients.get(0).size();

		for(int i=1; i<patients.size(); i++)
			minimum = Math.min(minimum, patients.get(i).size());

		for(int i=0; i<minimum; i++)
		{
			Signal avg = patients.get(0).read(i);

			for(int j=1; j<patients.size(); j++)
				avg.sum(patients.get(j).read(i));

			avg.div(patients.size());

			user.add(avg);
		}

		try
		{
			user.write(args[0]);
		}
		catch(IOException e)
		{
			System.out.println("Writing EKG failed");
			return;
		}
	}
}
