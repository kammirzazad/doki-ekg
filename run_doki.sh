#!/bin/sh

rm doki.log 2> /dev/null

timeout 60 \
java DOKI 0.1 10 \
./util/fakePatient1.txt \
data/patient001/1/1.txt \
data/patient001/2/2.txt \
data/patient001/3/3.txt \
data/patient002/1/4.txt \
data/patient003/1/5.txt \
data/patient004/1/6.txt \
data/patient004/2/7.txt \
data/patient005/1/8.txt \
data/patient005/2/9.txt \
data/patient005/3/10.txt \
data/patient005/4/11.txt \
data/patient005/5/12.txt \
data/patient006/1/13.txt \
data/patient006/2/14.txt \
data/patient006/3/15.txt \
data/patient007/1/16.txt \
data/patient007/2/17.txt \
data/patient007/3/18.txt \
data/patient007/4/19.txt \
data/patient008/1/20.txt \
data/patient008/2/21.txt \
data/patient008/3/22.txt \
data/patient009/1/23.txt \
data/patient010/1/24.txt \
data/patient010/2/25.txt \
data/patient010/3/26.txt \
> doki.log
