import	java.io.*;
import	java.util.*;	

public	class DOKI 
{
	public static EKG user;
	public static ArrayList<EKG> patients;
	public static int cnt_match_thr;

	public static void main(String args[])
	{
		if(args.length<4)
		{
			System.out.println("usage : [signal value match threshold (>=0,<1)] [signal count match threshold (>1)] [path_to_user_EKG] [path_to_patient_0_EKG] ...");
			System.exit(1);
		} 

		cnt_match_thr = Integer.parseInt(args[1]);
		Signal.val_match_thr = Float.parseFloat(args[0]);

		/* create user and patients ****/

		patients = new ArrayList<EKG>();

		try
		{
			user = new EKG(args[2]);

			for(int i=3; i<args.length; i++) {patients.add(new EKG(args[i]));}
		}
		catch(IOException e)
		{
			System.out.println("Creating EKGs failed");
			return;
		}

		/* start search ****************/

		int index = 0;

		while(index!=user.size()) 
		{ 
			//System.out.println("Starting search from "+Integer.toString(index));

			index = search(index); 
		}
				
		return;
	}

	public static int search(int start_index)
	{
		for(int threshold=15; threshold>6; threshold--)
		{
			int curr_index = start_index;

			for(int i=0; i<patients.size(); i++)
				patients.get(i).init_matches();

			int match_count = 100;

			while(match_count > 1)
			{
				match_count = 0;

				//System.out.println("Reading entry#"+Integer.toString(curr_index));

				Signal val = user.read(curr_index);

				for(int i=0; i<patients.size(); i++)
				{	
					patients.get(i).search_next(val,threshold);

					if(patients.get(i).anyMatch()) {match_count++;}
				}	

				curr_index++;		
			}

			// if there was no match, just lower threshold
			if(match_count==0) {continue;}		

			// lets find out where the match was 
			for(int i=0; i<patients.size(); i++)
			{
				if(patients.get(i).anyMatch())
				{	
					Integer match = patients.get(i).get_match();
	
					// lets make sure there is no more match
					while(user.is_valid(curr_index))
					{
						Signal val2 = user.read(curr_index);

						patients.get(i).search_next(val2,threshold);  

						curr_index++;

						if(!patients.get(i).anyMatch()) {break;}

						match = patients.get(i).get_match();
					}				

					/* there was not enough correlation */
					if((curr_index-start_index)<cnt_match_thr) { return curr_index; }

					System.out.println(patients.get(i).get_info() + " [Accuracy: " + Integer.toString(threshold) + "/15] [Index: " + Integer.toString(match) + "/" + Integer.toString(patients.get(i).size())+"] [#Match: " + Integer.toString(curr_index-start_index) + "]");	
					//System.out.println("Path: ["+args[3+i]+"]");

					return curr_index;
				}
			}
		}

		// no match with any accuracy was found, go to next sample
		return  start_index+1;	
	}
}
