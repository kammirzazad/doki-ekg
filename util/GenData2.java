import  java.io.*;
import  java.util.*;

public  class GenData2
{
	public static EKG user;
	public static ArrayList<EKG> patients;

	public static void main(String args[])
	{
		if(args.length<3)
		{
			System.out.println("usage : [path_to_output_EKG] [path_to_patient_0_EKG] [path_to_patient_1_EKG] ...");
			System.exit(1);
		}

		user = new EKG();
		patients = new ArrayList<EKG>();

                try
                {                    
                        for(int i=1; i<args.length; i++) {patients.add(new EKG(args[i]));}
                }
                catch(IOException e)
                {
                        System.out.println("Creating EKGs failed");
                        return;
                }	

		Integer minimum = patients.get(0).size();

		for(int i=1; i<patients.size(); i++)
			minimum = Math.min(minimum, patients.get(i).size());

		for(int i=0; i<minimum; )
		{			
			Random rn = new Random();

			Integer noise = rn.nextInt(8);
			Integer num_samples  = rn.nextInt(20)+1;
			Integer curr_patient = rn.nextInt(patients.size());

			Integer offset = patients.get(curr_patient).size()-num_samples;
			Integer start_index  = rn.nextInt(offset);			

			for(int j=0; j<num_samples; j++, i++)	
			{
				Signal val = patients.get(curr_patient).read(start_index+j);
				val.add_noise(noise);
				user.add(val);		
			}
		}

		try
		{
			user.write(args[0]);
		}
		catch(IOException e)
		{
			System.out.println("Writing EKG failed");
			return;
		}
	}
}
